package com.skipday.bitcoincalc;

/**
 * Created by jagamypriera on 16/05/15.
 */


        import android.app.Activity;
        import android.content.Context;
        import android.content.Intent;
        import android.content.pm.PackageManager;
        import android.net.Uri;
        import android.widget.Toast;

public final class BitcoinIntergration
{
    private static final String INTENT_EXTRA_PAYMENTREQUEST = "paymentrequest";
    private static final String INTENT_EXTRA_PAYMENT = "payment";
    private static final String INTENT_EXTRA_TRANSACTION_HASH = "transaction_hash";

    private static final String MIMETYPE_PAYMENTREQUEST = "application/bitcoin-paymentrequest"; // BIP 71
    public static void request(final Context context, final String address)
    {
        final Intent intent = makeBitcoinUriIntent(address, null);

        start(context, intent);
    }
    public static void request(final Context context, final String address, final long amount)
    {
        final Intent intent = makeBitcoinUriIntent(address, amount);

        start(context, intent);
    }
    public static void request(final Context context, final byte[] paymentRequest)
    {
        final Intent intent = makePaymentRequestIntent(paymentRequest);

        start(context, intent);
    }
    public static void requestForResult(final Activity activity, final int requestCode, final String address)
    {
        final Intent intent = makeBitcoinUriIntent(address, null);

        startForResult(activity, requestCode, intent);
    }
    public static void requestForResult(final Activity activity, final int requestCode, final String address, final long amount)
    {
        final Intent intent = makeBitcoinUriIntent(address, amount);

        startForResult(activity, requestCode, intent);
    }
    public static void requestForResult(final Activity activity, final int requestCode, final byte[] paymentRequest)
    {
        final Intent intent = makePaymentRequestIntent(paymentRequest);

        startForResult(activity, requestCode, intent);
    }
    public static byte[] paymentRequestFromIntent(final Intent intent)
    {
        final byte[] paymentRequest = intent.getByteArrayExtra(INTENT_EXTRA_PAYMENTREQUEST);

        return paymentRequest;
    }
    public static void paymentToResult(final Intent result, final byte[] payment)
    {
        result.putExtra(INTENT_EXTRA_PAYMENT, payment);
    }
    public static byte[] paymentFromResult(final Intent result)
    {
        final byte[] payment = result.getByteArrayExtra(INTENT_EXTRA_PAYMENT);

        return payment;
    }
    public static void transactionHashToResult(final Intent result, final String txHash)
    {
        result.putExtra(INTENT_EXTRA_TRANSACTION_HASH, txHash);
    }
    public static String transactionHashFromResult(final Intent result)
    {
        final String txHash = result.getStringExtra(INTENT_EXTRA_TRANSACTION_HASH);

        return txHash;
    }

    private static final int SATOSHIS_PER_COIN = 100000000;

    private static Intent makeBitcoinUriIntent(final String address, final Long amount)
    {
        final StringBuilder uri = new StringBuilder("bitcoin:");
        if (address != null)
            uri.append(address);
        if (amount != null)
            uri.append("?amount=").append(String.format("%d.%08d", amount / SATOSHIS_PER_COIN, amount % SATOSHIS_PER_COIN));

        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri.toString()));

        return intent;
    }

    private static Intent makePaymentRequestIntent(final byte[] paymentRequest)
    {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setType(MIMETYPE_PAYMENTREQUEST);
        intent.putExtra(INTENT_EXTRA_PAYMENTREQUEST, paymentRequest);

        return intent;
    }

    private static void start(final Context context, final Intent intent)
    {
        final PackageManager pm = context.getPackageManager();
        if (pm.resolveActivity(intent, 0) != null)
            context.startActivity(intent);
        else
            redirectToDownload(context);
    }

    private static void startForResult(final Activity activity, final int requestCode, final Intent intent)
    {
        final PackageManager pm = activity.getPackageManager();
        if (pm.resolveActivity(intent, 0) != null)
            activity.startActivityForResult(intent, requestCode);
        else
            redirectToDownload(activity);
    }

    private static void redirectToDownload(final Context context)
    {
        Toast.makeText(context, "No Bitcoin application found.\nPlease install Bitcoin Wallet.", Toast.LENGTH_LONG).show();

        final Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=de.schildbach.wallet"));
        final Intent binaryIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/schildbach/bitcoin-wallet/releases"));

        final PackageManager pm = context.getPackageManager();
        if (pm.resolveActivity(marketIntent, 0) != null)
            context.startActivity(marketIntent);
        else if (pm.resolveActivity(binaryIntent, 0) != null)
            context.startActivity(binaryIntent);
    }
}
