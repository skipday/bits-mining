package com.skipday.bitcoincalc;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import org.joda.time.DateTime;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;


public class MainActivity extends Activity {
    protected final int minute = 60, hour = minute * 60, day = hour * 24, MHS = 1, GHS = 2, THS = 3, PHS = 4, CURRENT_DIFF = 1, NEXT_DIFF = 2;
    protected final Handler handler = new Handler();
    protected EditText currentDiff, currentHashrate, currentExchange;
    protected Button hash, reload, thanks, misc;
    protected TextView currentBitDay, currentBitWeek, currentBitMonth, currentFiatDay, currentFiatWeek, currentFiatMonth, nextBitDay, nextBitWeek, nextBitMonth, nextFiatDay, nextFiatWeek, nextFiatMonth, nextDiffLabel, currDiffLabel;
    protected double fetchCurrentHashrate, fetchCurrentDifficulty, fecthCurrentUSDExchange, fecthNextDifficulty, reward, diffChange, eta, networkHash, totalBTC, nextBlockHeight, currentBlockHeight;
    protected int kindOfHash = 1;
    protected RelativeLayout parentLayout;
    protected ArrayList<String> listOfHash;
    protected View child;
    protected SharedPreferences prefs;
    protected Timer timer;
    protected TimerTask timerTask;
    protected String percDiffChange = "Next difficulty (" + ((diffChange > 0) ? ("+") : ("")) + String.format("%.2f", diffChange) + "%)";
    protected DateTime dt;
    protected boolean isPercentage = true, isConnected;
    private static final int REQUEST_CODE = 0;
    protected Typeface custom_font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong("SAVED_TIME", System.currentTimeMillis());
        editor.apply();
        listOfHash = new ArrayList<>();
        listOfHash.add("MH/s");
        listOfHash.add("GH/s");
        listOfHash.add("TH/s");
        listOfHash.add("PH/s");
        parentLayout = (RelativeLayout) findViewById(R.id.parent);
        currentBitDay = (TextView) findViewById(R.id.current_crypto_day);
        currentBitWeek = (TextView) findViewById(R.id.current_crypto_week);
        currentBitMonth = (TextView) findViewById(R.id.current_crypto_month);
        currentFiatDay = (TextView) findViewById(R.id.current_fiat_day);
        currentFiatWeek = (TextView) findViewById(R.id.current_fiat_week);
        currentFiatMonth = (TextView) findViewById(R.id.current_fiat_month);
        currentDiff = (EditText) findViewById(R.id.current_difficulty);
        currentHashrate = (EditText) findViewById(R.id.current_hashrate);
        currentExchange = (EditText) findViewById(R.id.current_exchange);
        currDiffLabel = (TextView) findViewById(R.id.update);

        nextBitDay = (TextView) findViewById(R.id.next_crypto_day);
        nextBitWeek = (TextView) findViewById(R.id.next_crypto_week);
        nextBitMonth = (TextView) findViewById(R.id.next_crypto_month);
        nextFiatDay = (TextView) findViewById(R.id.next_fiat_day);
        nextFiatWeek = (TextView) findViewById(R.id.next_fiat_week);
        nextFiatMonth = (TextView) findViewById(R.id.next_fiat_month);
        nextDiffLabel = (TextView) findViewById(R.id.next_diff_label);
        hash = (Button) findViewById(R.id.hash);
        reload = (Button) findViewById(R.id.reload);
        thanks = (Button) findViewById(R.id.thanks_to_me);
        misc = (Button) findViewById(R.id.misc);
        onClickHash();
        onClickNextDiff();
        onChangeHash();
        dt = new DateTime().dayOfMonth().withMaximumValue();
        TextView tx = (TextView) findViewById(R.id.title);
        TextView tx_shadow = (TextView) findViewById(R.id.title_shadow);

        custom_font = Typeface.createFromAsset(getAssets(),
                "fonts/Lighthouse_PersonalUse.ttf");
        tx.setTypeface(custom_font);
        tx_shadow.setTypeface(custom_font);

        thanks.setTypeface(custom_font);
        YoYo.with(Techniques.FadeIn).playOn(findViewById(R.id.parent));
        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new getData().execute();
            }
        });
        thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BitcoinIntergration.requestForResult(MainActivity.this, REQUEST_CODE, "12ruZjk7Goj2zBBh3UbqJ5SJwwLYkD1N2u");
            }
        });
        misc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                miscDialog();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        startTimer();
    }

    @Override
    public void onPause() {
        super.onPause();
        stoptimertask();
    }

    private void closeMenu() {
        parentLayout.removeView(child);
        child = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_about) {
            aboutDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void startTimer() {
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, 500, 120000);
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        new getData().execute();
                    }
                });
            }
        };
    }

    protected String getEasyReadValue(String value) {
        double getLong = Double.parseDouble(value.substring(0, value.indexOf('E')));
        double getDecimal = Double.parseDouble(value.substring(value.indexOf('E') + 1, value.length()));
        return new BigDecimal(String.format("%.2f", (Math.pow(getDecimal, getDecimal)) * getLong)).toPlainString();
    }

    public void stoptimertask() {
        if (timer != null) {
            timer.cancel();

            timer = null;
        }
    }

    protected void track(String message) {
        Log.d(getClass().getSimpleName(), message);
    }

    protected void onChangeHash() {
        currentHashrate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String formatting = "%.8f";
                if (currentHashrate.length() == 0 || currentHashrate.getText().toString().substring(0, 1).equals("."))
                    return;
                Float choosenHash = Float.valueOf(currentHashrate.getText().toString());
                if (Float.parseFloat(currentBitDay.getText().toString()) > 10) {
                    formatting = "%.0f";
                }
                closeMenu();
                currentBitDay.setText("" + String.format(formatting, (getBitValue(kindOfHash, choosenHash, CURRENT_DIFF) * day)));
                currentBitWeek.setText("" + String.format(formatting, (getBitValue(kindOfHash, choosenHash, CURRENT_DIFF) * day * 7)));
                currentBitMonth.setText("" + String.format(formatting, (getBitValue(kindOfHash, choosenHash, CURRENT_DIFF) * day * 30)));
                currentFiatDay.setText("" + String.format("%.2f", (fecthCurrentUSDExchange * (Float.parseFloat(currentBitDay.getText().toString())))));
                currentFiatWeek.setText("" + String.format("%.2f", (fecthCurrentUSDExchange * (Float.parseFloat(currentBitWeek.getText().toString())))));
                currentFiatMonth.setText("" + String.format("%.2f", (fecthCurrentUSDExchange * (Float.parseFloat(currentBitMonth.getText().toString())))));

                nextBitDay.setText("" + String.format(formatting, (getBitValue(kindOfHash, choosenHash, NEXT_DIFF) * day)));
                nextBitWeek.setText("" + String.format(formatting, (getBitValue(kindOfHash, choosenHash, NEXT_DIFF) * day * 7)));
                nextBitMonth.setText("" + String.format(formatting, (getBitValue(kindOfHash, choosenHash, NEXT_DIFF) * day * 30)));
                nextFiatDay.setText("" + String.format("%.2f", (fecthCurrentUSDExchange * (Float.parseFloat(nextBitDay.getText().toString())))));
                nextFiatWeek.setText("" + String.format("%.2f", (fecthCurrentUSDExchange * (Float.parseFloat(nextBitWeek.getText().toString())))));
                nextFiatMonth.setText("" + String.format("%.2f", (fecthCurrentUSDExchange * (Float.parseFloat(nextBitMonth.getText().toString())))));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    protected void onClickHash() {
        hash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (child == null) {
                    DisplayMetrics metrics = MainActivity.this.getResources().getDisplayMetrics();
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(v.getWidth(), RelativeLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.leftMargin = metrics.widthPixels - (v.getWidth() + findViewById(R.id.relativeLayout).getPaddingRight());
                    layoutParams.topMargin = findViewById(R.id.current_diff_label).getHeight() + findViewById(R.id.current_difficulty).getHeight() + findViewById(R.id.current_difficulty).getPaddingBottom() + findViewById(R.id.b).getHeight() + findViewById(R.id.current_hashrate).getHeight() + (findViewById(R.id.current_hashrate).getPaddingBottom() * 4) + findViewById(R.id.rl1).getHeight() + findViewById(R.id.rl1).getPaddingTop();
                    layoutParams.width = v.getWidth();
                    child = getLayoutInflater().inflate(R.layout.listview_menu, null);
                    final ListView lv = (ListView) child.findViewById(R.id.menu);
                    ArrayAdapter<String> menu = new ArrayAdapter<String>(MainActivity.this, R.layout.menu_item, listOfHash) {
                        @Override
                        public View getView(final int position, View convertView, ViewGroup parent) {
                            if (convertView == null) {
                                convertView = getLayoutInflater().inflate(R.layout.menu_item, null);
                            }
                            TextView tradingSiteName = (TextView) convertView.findViewById(R.id.menuName);

                            tradingSiteName.setText(listOfHash.get(position));
                            return convertView;
                        }
                    };
                    lv.setAdapter(menu);
                    lv.setSelector(R.drawable.selector);
                    lv.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
                    parentLayout.addView(child, layoutParams);
                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            closeMenu();
                            switch (position + 1) {
                                case MHS:
                                    kindOfHash = MHS;
                                    hash.setText("MH/s");
                                    break;
                                case GHS:
                                    kindOfHash = GHS;
                                    hash.setText("GH/s");
                                    break;
                                case THS:
                                    kindOfHash = THS;
                                    hash.setText("TH/s");
                                    break;
                                case PHS:
                                    kindOfHash = PHS;
                                    hash.setText("PH/s");
                                    break;
                            }
                        }
                    });
                } else {
                    closeMenu();
                }

            }
        });
    }

    protected double getBitValue(int hash, float choosenHash, int which) {
        int khs = 1000;
        double choosenDiff = 0;
        long returnHash = 0;
        switch (hash) {
            case MHS: {
                returnHash = 1000l * khs;
            }
            break;
            case GHS: {
                returnHash = 1000000l * khs;
            }
            break;
            case THS: {
                returnHash = 1000000000l * khs;
            }
            break;
            case PHS: {
                returnHash = 1000000000000l * khs;
            }
            break;
        }
        switch (which) {
            case CURRENT_DIFF:
                choosenDiff = fetchCurrentDifficulty;
                break;
            case NEXT_DIFF:
                choosenDiff = fecthNextDifficulty;
                break;
        }
        return reward * returnHash * choosenHash / (choosenDiff * (Math.pow(2, 32)));
    }

    protected void onClickNextDiff() {
        nextDiffLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPercentage) {
                    YoYo.with(Techniques.FadeIn).playOn(v);
                    nextDiffLabel.setText(" (" + getEasyReadValue(String.valueOf(fecthNextDifficulty)) + ")");
                    isPercentage = false;
                } else {
                    YoYo.with(Techniques.FadeIn).playOn(v);
                    nextDiffLabel.setText(" (" + ((diffChange > 0) ? ("+") : ("")) + String.format("%.2f", diffChange) + "% eta " + calculateTime(eta) + ")");
                    isPercentage = true;
                }
            }
        });
    }

    private boolean isReacheble() {
        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL("https://blockchain.info/").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            //urlc.setConnectTimeout(1500);
            urlc.connect();
            return (urlc.getResponseCode() == 200);
        } catch (IOException e) {
            Log.e(getClass().getSimpleName(), "Error checking internet connection", e);
        }
        return false;
    }

    private class getData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            reload.setText("Loading...");
            reload.setEnabled(false);
            misc.setEnabled(false);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            track("REACH " + isReacheble());
            if (isReacheble()) {
                fetchCurrentDifficulty = FetchData.callURL("https://blockchain.info/q/getdifficulty");
                fetchCurrentHashrate = FetchData.callURL("https://blockchain.info/q/hashrate");
                fecthCurrentUSDExchange = 1 / FetchData.callURL("https://blockchain.info/tobtc?currency=USD&value=1");
                fecthNextDifficulty = FetchData.callURL("http://blockexplorer.com/q/estimate");
                reward = FetchData.callURL("https://blockchain.info/q/bcperblock") / 100000000;
                eta = FetchData.callURL("https://blockexplorer.com/q/eta");
                diffChange = (fecthNextDifficulty - fetchCurrentDifficulty) / fetchCurrentDifficulty * 100;

                isConnected = true;
                currentBlockHeight = FetchData.callURL("https://blockchain.info/q/getblockcount");
                nextBlockHeight = FetchData.callURL("https://blockchain.info/q/nextretarget");
                totalBTC = FetchData.callURL("https://blockchain.info/q/totalbc");
            } else {
                fetchCurrentDifficulty = 0;
                fetchCurrentHashrate = 0;
                fecthCurrentUSDExchange = 0;
                fecthNextDifficulty = 0;
                reward = 0;
                diffChange = 0;
                eta = 0;
                isConnected = false;
                currentBlockHeight = 0;
                nextBlockHeight = 0;
                totalBTC = 0;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (isConnected) {
                Log.d(getClass().getSimpleName(), "Respons Hashrate " + fetchCurrentHashrate + " Difficulty " + fetchCurrentDifficulty + " Next Difficulty " + fecthNextDifficulty);
                currentExchange.setText("" + String.format("%.2f", fecthCurrentUSDExchange));
                currentDiff.setText("" + getEasyReadValue(String.valueOf(fetchCurrentDifficulty)));
                nextDiffLabel.setText(" (" + ((diffChange > 0) ? ("+") : ("")) + String.format("%.2f", diffChange) + "% eta " + calculateTime(eta) + ")");
                SimpleDateFormat fmt = new SimpleDateFormat("HH:mm:ss");
                Date date = new Date();
                String dateString = fmt.format(date);
                currDiffLabel.setText("Updated: Today at " + dateString);
                reload.setText("Reload");
                track(dateString);
            } else {
                Toast.makeText(MainActivity.this, "Cannot connect to server", Toast.LENGTH_SHORT).show();
            }
            reload.setEnabled(true);
            misc.setEnabled(true);
            super.onPostExecute(result);
        }
    }

    public String calculateTime(double seconds) {
        int day = (int) TimeUnit.SECONDS.toDays((long) seconds);
        long hours = TimeUnit.SECONDS.toHours((long) seconds) -
                TimeUnit.DAYS.toHours(day);
        long minute = TimeUnit.SECONDS.toMinutes((long) seconds) -
                TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours((long) seconds));
        long second = TimeUnit.SECONDS.toSeconds((long) seconds) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes((long) seconds));
        return day + "d " + hours + "h " + minute + "m " + second + "s";
    }

    private void aboutDialog() {
        Dialog about = new Dialog(MainActivity.this);
        about.requestWindowFeature(Window.FEATURE_NO_TITLE);
        about.setContentView(R.layout.about);
        about.findViewById(R.id.aboutly).setBackgroundColor(Color.argb(255, 23, 23, 23));
        PackageManager pm = this.getPackageManager();
        assert pm != null;
        ApplicationInfo installAt = null;
        PackageInfo versi = null;
        try {
            installAt = pm.getApplicationInfo(getPackageName(), 0);
            versi = pm.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        }
        assert installAt != null;
        assert versi != null;
        String appFile = installAt.sourceDir, namaVersi = versi.versionName, kodeVersi = String.valueOf(versi.versionCode);
        long installed = new File(appFile).lastModified();
        ImageButton likeUs = (ImageButton) about.findViewById(R.id.idLikeUs);
        likeUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://search?q=pname:" + getPackageName()));
                startActivity(intent);
            }
        });
        TextView label = (TextView) about.findViewById(R.id.idTitle);
        TextView aboutText = (TextView) about.findViewById(R.id.aboutText);
        aboutText.setText(getString(R.string.about_text, "" + pm.getApplicationLabel(installAt)));
        TextView detailLabel = (TextView) about.findViewById(R.id.idDetailTitle);
        label.setText("" + pm.getApplicationLabel(installAt));
        detailLabel.setText("" + pm.getApplicationLabel(installAt));
        TextView installedDate = (TextView) about.findViewById(R.id.iDate);
        installedDate.setText("" + dateModif(installed));
        TextView version = (TextView) about.findViewById(R.id.version);
        version.setText("" + namaVersi + " (" + kodeVersi + ")");
        about.show();
    }

    public String dateModif(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("MMM d, yyyy  HH:mm");
        return formatter.format(new Date(date));
    }

    private void miscDialog() {
        Dialog about = new Dialog(MainActivity.this);
        about.requestWindowFeature(Window.FEATURE_NO_TITLE);
        about.setContentView(R.layout.misc);
        TextView reward = (TextView) about.findViewById(R.id.reward);
        TextView nextBlockHeight = (TextView) about.findViewById(R.id.next_block_height);
        TextView currentBlockHeight = (TextView) about.findViewById(R.id.current_block_height);
        TextView networkHash = (TextView) about.findViewById(R.id.network_hash);
        TextView totalBTC = (TextView) about.findViewById(R.id.total_btc);
        TextView miscTitle = (TextView) about.findViewById(R.id.misc_title);
        miscTitle.setTypeface(custom_font);
        reward.setText("" + String.format("%.2f", MainActivity.this.reward));
        nextBlockHeight.setText("" + String.format("%.0f",MainActivity.this.nextBlockHeight));
        currentBlockHeight.setText("" + String.format("%.0f",MainActivity.this.currentBlockHeight));
        networkHash.setText("" + getEasyReadValue(String.valueOf(MainActivity.this.fetchCurrentHashrate)) + " GH/s");
        totalBTC.setText("" + getEasyReadValue(String.valueOf(MainActivity.this.totalBTC)).substring(0,getEasyReadValue(String.valueOf(MainActivity.this.totalBTC)).lastIndexOf('.')) );
                about.show();
    }
}
